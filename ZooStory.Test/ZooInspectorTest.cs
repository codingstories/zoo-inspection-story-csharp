﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ZooStory.Interfaces;

namespace ZooStory.Test
{
    [TestClass]
    public class ZooInspectorTest
    {
        private const string ZooId = "zoo";
        private const string TestEnclosureId = "enclosure";
        private const string TestAnimalName = "animal";

        private Mock<IZoo> zoo = new Mock<IZoo>();

        private Mock<IImageRecognitionSystem> imageRecognitionSystem = new Mock<IImageRecognitionSystem>();

        private InspectionLogStub inspectionLog;

        private ZooInspector zooInspector;

        [TestInitialize]
        public void SetUp()
        {
            zoo.Setup(z => z.GetId()).Returns(ZooId);
            inspectionLog = new InspectionLogStub();
            zooInspector = new ZooInspector(imageRecognitionSystem.Object, inspectionLog);
        }

        [TestMethod]
        public void WhenNoAnimals_ThenNoRequests()
        {
            //given
            List<IEnclosure> noEnclosures = new List<IEnclosure>();
            zoo.Setup(z => z.GetEnclosures()).Returns(noEnclosures);

            //when
            zooInspector.Inspect(zoo.Object);

            //then
            zoo.Verify(z => z.RequestMaintenanceCrewTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestSecurityTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestVeterinaryTo(It.IsAny<IAnimal>()), Times.Never());
            zoo.Verify(z => z.CloseEnclosure(It.IsAny<IEnclosure>()), Times.Never());

            VerifyLog("ZOO#" + ZooId + "#" + "OK");
        }

        [TestMethod]
        public void WhenEnclosureAndAnimalAlright_ThenNoRequests()
        {
            //given
            Mock<IEnclosure> enclosure = new Mock<IEnclosure>();
            Mock<IAnimal> animal = new Mock<IAnimal>();
            enclosure.Setup(e => e.GetAnimal()).Returns(animal.Object);

            List<IEnclosure> enclosures = new List<IEnclosure>() { enclosure.Object };
            zoo.Setup(z => z.GetEnclosures()).Returns(enclosures);

            Mock<IAnimalStatus> animalStatus = new Mock<IAnimalStatus>();
            Mock<IEnclosureStatus> enclosureStatus = new Mock<IEnclosureStatus>();
            imageRecognitionSystem.Setup(i => i.RecognizeAnimalStatus(It.IsAny<IAnimal>(), It.IsAny<IImage>())).Returns(animalStatus.Object);
            imageRecognitionSystem.Setup(i => i.RecognizeEnclosureStatus(It.IsAny<IEnclosure>(), It.IsAny<IImage>())).Returns(enclosureStatus.Object);
            enclosureStatus.Setup(e => e.IsEnclosureSafe()).Returns(true);
            animalStatus.Setup(a => a.IsAnimalSick()).Returns(false);

            // when
            zooInspector.Inspect(zoo.Object);

            // then
            zoo.Verify(z => z.RequestMaintenanceCrewTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestSecurityTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestVeterinaryTo(It.IsAny<IAnimal>()), Times.Never());
            zoo.Verify(z => z.CloseEnclosure(It.IsAny<IEnclosure>()), Times.Never());

            VerifyLog("ZOO#" + ZooId + "#" + "OK");
        }

        [TestMethod]
        public void WhenEnclosureIsNotSafe_ThenRequests()
        {
            // given
            Mock<IAnimal> animal = new Mock<IAnimal>();
            Mock<IEnclosure> enclosure = new Mock<IEnclosure>();
            enclosure.Setup(e => e.GetAnimal()).Returns(animal.Object);
            enclosure.Setup(e => e.GetId()).Returns(TestEnclosureId);

            List<IEnclosure> enclosures = new List<IEnclosure>() { enclosure.Object };
            zoo.Setup(z => z.GetEnclosures()).Returns(enclosures);

            Mock<IAnimalStatus> animalStatus = new Mock<IAnimalStatus>();
            animalStatus.Setup(a => a.IsAnimalSick()).Returns(false);
            Mock<IEnclosureStatus> enclosureStatus = new Mock<IEnclosureStatus>();
            enclosureStatus.Setup(e => e.IsEnclosureSafe()).Returns(false);

            imageRecognitionSystem.Setup(i => i.RecognizeAnimalStatus(It.IsAny<IAnimal>(), It.IsAny<IImage>())).Returns(animalStatus.Object);
            imageRecognitionSystem.Setup(i => i.RecognizeEnclosureStatus(It.IsAny<IEnclosure>(), It.IsAny<IImage>())).Returns(enclosureStatus.Object);

            // when
            zooInspector.Inspect(zoo.Object);

            // then
            zoo.Verify(z => z.RequestMaintenanceCrewTo(It.IsAny<IEnclosure>()));
            zoo.Verify(z => z.RequestSecurityTo(It.IsAny<IEnclosure>()));
            zoo.Verify(z => z.CloseEnclosure(It.IsAny<IEnclosure>()));
            // but animal is fine
            zoo.Verify(z => z.RequestVeterinaryTo(It.IsAny<IAnimal>()), Times.Never());

            VerifyLog("ENCLOSURE#" + TestEnclosureId + "#WARNING", "ZOO#" + ZooId + "#" + "WARNING");
        }

        [TestMethod]
        public void WhenAnimalIsSick_ThenRequests()
        {
            // given
            Mock<IAnimal> animal = new Mock<IAnimal>();
            animal.Setup(a => a.GetName()).Returns(TestAnimalName);

            Mock<IEnclosure> enclosure = new Mock<IEnclosure>();
            enclosure.Setup(e => e.GetAnimal()).Returns(animal.Object);

            List<IEnclosure> noEnclosures = new List<IEnclosure>() { enclosure.Object };
            zoo.Setup(z => z.GetEnclosures()).Returns(noEnclosures);

            Mock<IAnimalStatus> animalStatus = new Mock<IAnimalStatus>();
            animalStatus.Setup(a => a.IsAnimalSick()).Returns(true);

            Mock<IEnclosureStatus> enclosureStatus = new Mock<IEnclosureStatus>();
            enclosureStatus.Setup(e => e.IsEnclosureSafe()).Returns(true);

            imageRecognitionSystem.Setup(i => i.RecognizeAnimalStatus(It.IsAny<IAnimal>(), It.IsAny<IImage>())).Returns(animalStatus.Object);
            imageRecognitionSystem.Setup(i => i.RecognizeEnclosureStatus(It.IsAny<IEnclosure>(), It.IsAny<IImage>())).Returns(enclosureStatus.Object);

            // when
            zooInspector.Inspect(zoo.Object);

            // then
            zoo.Verify(z => z.CloseEnclosure(It.IsAny<IEnclosure>()));
            zoo.Verify(z => z.RequestVeterinaryTo(It.IsAny<IAnimal>()));
            // but enclosure is fine
            zoo.Verify(z => z.RequestMaintenanceCrewTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestSecurityTo(It.IsAny<IEnclosure>()), Times.Never());

            VerifyLog("ANIMAL#" + TestAnimalName + "#WARNING", "ZOO#" + ZooId + "#" + "WARNING");
        }

        [TestMethod]
        public void WhenFirstAnimalIsSickAndSecondIsHealth_ThenStillRequest()
        {
            // given
            Mock<IAnimal> sickAnimal = new Mock<IAnimal>() { Name = "Sick animal" };
            sickAnimal.Setup(s => s.GetName()).Returns("SICK_ANIMAL");
            Mock<IEnclosure> enclosureWithSickAnimal = new Mock<IEnclosure>() { Name = "Enclosure with sick animal" };
            enclosureWithSickAnimal.Setup(e => e.GetAnimal()).Returns(sickAnimal.Object);

            Mock<IAnimal> healthyAnimal = new Mock<IAnimal>() { Name = "Healthy animal" };
            healthyAnimal.Setup(s => s.GetName()).Returns("HEALTHY_ANIMAL");
            Mock<IEnclosure> enclosureWithHealthyAnimal = new Mock<IEnclosure>() { Name = "Enclosure with healthy animal" };
            enclosureWithHealthyAnimal.Setup(e => e.GetAnimal()).Returns(healthyAnimal.Object);

            List<IEnclosure> enclosures = new List<IEnclosure>() { enclosureWithSickAnimal.Object, enclosureWithHealthyAnimal.Object };
            zoo.Setup(z => z.GetEnclosures()).Returns(enclosures);

            Mock<IAnimalStatus> sickAnimalStatus = new Mock<IAnimalStatus>() { Name = "Sick animal status" };
            sickAnimalStatus.Setup(a => a.IsAnimalSick()).Returns(true);
            Mock<IAnimalStatus> healthyAnimalStatus = new Mock<IAnimalStatus>() { Name = "Healthy animal status" };
            healthyAnimalStatus.Setup(a => a.IsAnimalSick()).Returns(false);
            Mock<IEnclosureStatus> enclosureStatus = new Mock<IEnclosureStatus>();
            enclosureStatus.Setup(e => e.IsEnclosureSafe()).Returns(true);

            imageRecognitionSystem.Setup(i => i.RecognizeAnimalStatus(sickAnimal.Object, It.IsAny<IImage>())).Returns(sickAnimalStatus.Object);
            imageRecognitionSystem.Setup(i => i.RecognizeAnimalStatus(healthyAnimal.Object, It.IsAny<IImage>())).Returns(healthyAnimalStatus.Object);
            imageRecognitionSystem.Setup(i => i.RecognizeEnclosureStatus(It.IsAny<IEnclosure>(), It.IsAny<IImage>())).Returns(enclosureStatus.Object);

            // when
            zooInspector.Inspect(zoo.Object);

            // then
            zoo.Verify(z => z.CloseEnclosure(enclosureWithSickAnimal.Object));
            zoo.Verify(z => z.RequestVeterinaryTo(sickAnimal.Object));
            // but enclosure is fine
            zoo.Verify(z => z.RequestMaintenanceCrewTo(It.IsAny<IEnclosure>()), Times.Never());
            zoo.Verify(z => z.RequestSecurityTo(It.IsAny<IEnclosure>()), Times.Never());

            VerifyLog("ANIMAL#" + "SICK_ANIMAL" + "#WARNING", "ZOO#" + ZooId + "#" + "WARNING");
        }

        private void VerifyLog(params string[] statuses)
        {
            IEnumerable<string> expectedStatuses = statuses;
            IEnumerable<string> actualStatuses = inspectionLog.GetStatuses();
            Assert.IsTrue(expectedStatuses.SequenceEqual(actualStatuses));
        }


        private class InspectionLogStub : IInspectionLog
        {

            private IEnumerable<string> _statuses;

            public void Log(IEnumerable<string> statuses)
            {
                this._statuses = statuses;
            }

            public IEnumerable<string> GetStatuses()
            {
                return _statuses;
            }
        }
    }
}
