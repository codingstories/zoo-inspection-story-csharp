﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using ZooStory.Interfaces;

namespace ZooStory
{
    public class ZooInspector
    {
        private readonly IImageRecognitionSystem ImageRecognitionSystem;
        private readonly IInspectionLog InspectionLog;

        public ZooInspector(IImageRecognitionSystem imageRecognitionSystem, IInspectionLog inspectionLog)
        {
            this.ImageRecognitionSystem = imageRecognitionSystem;
            this.InspectionLog = inspectionLog;
        }

        public void Inspect(IZoo zoo)
        {
            Inspection inspection = new Inspection(this, zoo);
            inspection.RunInspection();
            InspectionLog.Log(inspection.GetStatuses());
        }

        private class Inspection
        {
            private const string WarningStatus = "WARNING";
            private const string OkStatus = "OK";
            private const string ZooStatusName = "ZOO";
            private const string EnclosureStatusName = "ENCLOSURE";
            private const string AnimalStatusStatus = "ANIMAL";

            private readonly ZooInspector ZooInspector;
            private readonly IZoo Zoo;
            
            private readonly List<string> InspectionStatuses;

            internal Inspection(ZooInspector zooInspector, IZoo zoo)
            {
                this.ZooInspector = zooInspector;
                this.Zoo = zoo;
                this.InspectionStatuses = new List<string>();
            }

            internal void RunInspection()
            {
                InspectEnclosuresAndAnimals();
                ReportZooStatus();
            }

            private void InspectEnclosuresAndAnimals()
            {
                foreach (IEnclosure enclosure in Zoo.GetEnclosures())
                {
                    InspectEnclosure(enclosure);
                    InspectAnimal(enclosure);
                }
            }
            
            private void ReportZooStatus()
            {
                string zooStatus = IsZooInWarningStatus() ? WarningStatus : OkStatus;
                ReportStatus(ZooStatusName, Zoo.GetId(), zooStatus);
            }

            private bool IsZooInWarningStatus()
            {
                return IsNotEmptyInspectionStatuses();
            }

            private bool IsNotEmptyInspectionStatuses()
            {
                return InspectionStatuses.Any();
            }
            
            private void ReportEnclosureWarningStatus(IEnclosure enclosure)
            {
                ReportStatus(EnclosureStatusName, enclosure.GetId(), WarningStatus);
            }

            private void ReportAnimalWarningStatus(IAnimal animal)
            {
                ReportStatus(AnimalStatusStatus, animal.GetName(), WarningStatus);
            }

            private void ReportStatus(string objectName, string objectId, string status)
            {
                InspectionStatuses.Add($"{objectName}#{objectId}#{status}");
            }

            private void InspectAnimal(IEnclosure enclosure)
            {
                IImage animalImage = Zoo.CapturePictureOf(enclosure.GetAnimal());
                IAnimalStatus animalStatus = ZooInspector.ImageRecognitionSystem.RecognizeAnimalStatus(enclosure.GetAnimal(), animalImage);
                if (animalStatus.IsAnimalSick())
                {
                    RespondToSickAnimal(enclosure);
                    ReportSickAnimalStatus(enclosure.GetAnimal());
                }
            }

            private void RespondToSickAnimal(IEnclosure enclosure)
            {
                Zoo.CloseEnclosure(enclosure);
                Zoo.RequestVeterinaryTo(enclosure.GetAnimal());
            }

            private void ReportSickAnimalStatus(IAnimal animal)
            {
                ReportAnimalWarningStatus(animal);
            }

            private void InspectEnclosure(IEnclosure enclosure)
            {
                IImage enclosureImage = Zoo.CapturePictureOf(enclosure);
                IEnclosureStatus enclosureStatus = ZooInspector.ImageRecognitionSystem.RecognizeEnclosureStatus(enclosure, enclosureImage);
                if (IsNotSafeEnclosure(enclosureStatus))
                {
                    RespondToNotSafeEnclosure(enclosure);
                    ReportEnclosureWarningStatus(enclosure);
                }
            }

            private void RespondToNotSafeEnclosure(IEnclosure enclosure)
            {
                Zoo.CloseEnclosure(enclosure);
                Zoo.RequestSecurityTo(enclosure);
                Zoo.RequestMaintenanceCrewTo(enclosure);
            }
            
            private static bool IsNotSafeEnclosure(IEnclosureStatus enclosureStatus)
            {
                return !enclosureStatus.IsEnclosureSafe();
            }

            internal List<string> GetStatuses()
            {
                return this.InspectionStatuses;
            }
        }
    }
}
