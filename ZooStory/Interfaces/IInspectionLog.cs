﻿using System.Collections.Generic;

namespace ZooStory.Interfaces
{
    public interface IInspectionLog
    {
        void Log(IEnumerable<string> statuses);
    }
}
