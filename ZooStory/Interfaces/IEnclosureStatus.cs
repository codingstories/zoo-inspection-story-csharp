﻿namespace ZooStory.Interfaces
{
    public interface IEnclosureStatus
    {
        bool IsEnclosureSafe();
    }
}
