﻿namespace ZooStory.Interfaces
{
    public interface IAnimalStatus
    {
        bool IsAnimalSick();
    }
}
