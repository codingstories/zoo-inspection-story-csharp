﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace ZooStory.Interfaces
{
    public interface IEnclosure
    {
        IAnimal GetAnimal();
        string GetId();
    }
}
