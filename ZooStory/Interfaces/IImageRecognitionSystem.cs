﻿namespace ZooStory.Interfaces
{
    public interface IImageRecognitionSystem
    {
        IAnimalStatus RecognizeAnimalStatus(IAnimal animal, IImage animalImage);

        IEnclosureStatus RecognizeEnclosureStatus(IEnclosure enclosure, IImage enclosureImage);
    }
}
