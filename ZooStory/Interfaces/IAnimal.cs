﻿namespace ZooStory.Interfaces
{
    public interface IAnimal
    {
        string GetName();

    }
}
