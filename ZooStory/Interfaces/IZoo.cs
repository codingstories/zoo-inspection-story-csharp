﻿using System.Collections.Generic;

namespace ZooStory.Interfaces
{
    public interface IZoo
    {
        string GetId();

        IEnumerable<IEnclosure> GetEnclosures();

        void CloseEnclosure(IEnclosure enclosure);

        void RequestSecurityTo(IEnclosure enclosure);

        void RequestMaintenanceCrewTo(IEnclosure enclosure);

        void RequestVeterinaryTo(IAnimal animal);

        IImage CapturePictureOf(IAnimal animal);

        IImage CapturePictureOf(IEnclosure enclosure);
    }
}
