# Zoo Inspection Story

**To read**: [https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fzoo-inspection-story-csharp]

## Story Outline
This story is about automated inspection system for zoo which uses image recognition system to check animal health and safety of visitors. We are going review one unusual violation of *Do Not Repeat Yourself* principle.

## Story Organization
**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**:task
>`git checkout task`

Tags: #clean_code